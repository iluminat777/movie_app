import React, { Component } from 'react';
import Loader from './Loader.js'
import {Link} from 'react-router-dom';
// import Cancel from './cancel.svg'
import Pagination from "react-js-pagination";
import PropTypes from 'prop-types';
// import Filter from './Filter.js'

class ListSeries extends Component {

    HandletOnChange = (pageNumber) => {
        this.props.getListSeries(pageNumber)
    };

    componentDidMount() {
        this.props.getListSeries(this.props.ListSeries.page)
    };

    render () {
        let {data, loaded} = this.props.ListSeries;
        if (loaded === true) {

            if (this.props.location.state !== undefined) {
                // Filter(this.props.location.state, data)
                let {filter} = this.props.location.state;

                if (filter === 'name') {
                    data.sort(function (a, b){
                        if (a.title > b.title) {
                            return 1;
                        }
                        if (a.title < b.title) {
                            return -1
                        }
                        return 0
                    })
                }

                if (filter === 'raiting') {
                    data.sort(function (a, b)
                    {
                        return b.vote_average - a.vote_average;
                    })
                }

                if (filter === 'runtime') {
                    data.sort(function (a, b)
                    {
                        return b.popularity - a.popularity;
                    })
                }
                console.log(data)
            }

            return(
                <section>
                    <div className="block p-0">
                        <div className="inner">
                            <div className="row columns-spaced">
                                {data.map(item => (
                                    <div key={item.id} className="col-xl-6 col-l-8 col-m-12 col-s-24">
                                        <div className="box-item">
                                            <figure className="rollover">
                                                <div className="box-photo box-bg">
                                                    <Link to={`/tv/${item.id}`}>
                                                        <img className="lazy lazy-loaded" src={item.backdrop_path === null ? './no_image.png' : `https://image.tmdb.org/t/p/w500/${item.backdrop_path}`} alt="img_movie"/>
                                                    </Link>
                                                    <div className="hover-item bt-item">
                                                        <Link className="link bt-link" to={`/tv/${item.id}`} target="_blank" rel="noopener nofollow">
                                                            <img src="image/link.svg" alt=""/>
                                                        </Link>
                                                    </div>
                                                </div>
                                            </figure>
                                            <div className="box-info">
                                                <div className="content">
                                                    <h3 className="title-h3">
                                                        <Link  className="link"  to={`/tv/${item.id}`}>
                                                            {item.name}
                                                        </Link>
                                                    </h3>
                                                </div>
                                                <div className="footer"></div>
                                            </div>
                                        </div>
                                    </div>
                                ))}
                            </div>
                        </div>
                    </div>
                    <div className="block">
                        <div className="inner">
                            <Pagination
                                innerClass={"paginate"}
                                activeClass={"current"}
                                itemClass={"item"}
                                activePage={this.props.ListSeries.page}
                                itemsCountPerPage={1}
                                totalItemsCount={this.props.ListSeries.totalpages}
                                pageRangeDisplayed={5}
                                onChange={this.HandletOnChange}
                            />
                        </div>
                    </div>
                </section>
            );
        }
        else return ( <Loader/> )
    }
};

ListSeries.propTypes = {
    ListSeries: PropTypes.object,
    getListSeries: PropTypes.func,
}

export default ListSeries;



//<Link key={item.id} to={`/movie/${item.id}`}>  {item.title}</Link> {item.overview} <img className="B right material-icons" src={Cancel} alt="loading"/>