import React, { Component } from 'react';
import Header from './layouts/Header.js';
import Hero from './layouts/Hero.js';
import Main from './layouts/Main.js';
import Footer from './layouts/Footer.js';

class App extends Component {
    render() {
        return (
            <div className="g-container is-header-fixed ">
                <Header/>
                <Main/>
            </div>
        );
    }
}

export default App;
