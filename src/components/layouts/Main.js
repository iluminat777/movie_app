import React, { Component } from 'react';
import MyListMovie from '../../containers/ListMovieContainer'
import Movie from '../../containers/MovieContainer';
import MyListSeries from '../../containers/ListSeriesContainer'
import Series from '../../containers/SeriesContainer';
import { BrowserRouter, Switch, Route, Redirect } from 'react-router-dom';

const supportsHistory = 'pushState' in window.history;

class Main extends Component {
    render() {
        return (
            <main className="g-content">
                <BrowserRouter basename="/" forceRefrech={!supportsHistory}>
                    <Switch>
                        <Route path="/movie" exact render={(props) => {
                            return <MyListMovie {...props}/>
                        }} />

                        <Route path="/movie/:id" exact render={({match}) => {
                            return <Movie id={match.params.id}/>
                        }} />

                        <Route path="/tv" exact render={(props) => {
                            return <MyListSeries {...props}/>
                        }} />

                        <Route path="/tv/:id" exact render={({match}) => {
                            return <Series id={match.params.id}/>
                        }} />
                        {/*<Redirect from='/' to='/movie'/>*/}
                    </Switch>
                </BrowserRouter>
            </main>
        )
    }
}


export default Main;









