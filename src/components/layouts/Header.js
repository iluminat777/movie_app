import React, { Component } from 'react';



class Header extends Component {
    render() {
          return ( 
              <header className="g-header is-fixed ">
                  <div className="header-content">
                      <div className="inner">
                          <div className="header-logo">logo</div>
                          <div className="header-menu">
                              <nav>
                                  <ul>
                                      <li>
                                          <a href="/">Главная </a>
                                      </li>
                                      <li>
                                          <a href="/movie">Фильмы</a>
                                      </li>
                                      <li>
                                          <a href="/tv">Сериалы</a>
                                      </li>
                                  </ul>
                              </nav>
                          </div>
                      </div>
                  </div>
                  <div className="bar search-bar">
                      <div className="inner">
                          <form action="" className="search-form">Form</form>
                      </div>
                  </div>
              </header>
          )
    }
}


export default Header;









