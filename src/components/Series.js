import React, { Component } from 'react';
import Loader from './Loader.js'
import ReactStars from 'react-stars'
import PropTypes from 'prop-types';

class Series extends Component {

    componentDidMount() {
        this.props.getSeries(this.props.id);
    };


    render() {
        let {data, loaded} = this.props.Series;
        let releaseData = data.first_air_date;
        let imgBG = `https://image.tmdb.org/t/p/w500/${data.backdrop_path}`;
        const divStyle = {
            position: 'absolute',
            left: '0',
            right: '0',
            height: '100%',
            backgroundSize: 'cover',
            backgroundRepeat: 'no-repeat',
            backgroundPosition: '50% 50%',
            backgroundImage: 'url(' + imgBG + ')',
            willChange: 'opacity',
            transition: 'filter 1s',
            filter: 'opacity(100) grayscale(100%) contrast(130%)'
        };
        console.log(data);
        console.log(loaded === true);

        if (loaded === true) {
            return (
                <section>
                    <div className="g-hero _Series-hero">
                        <div className="hero-view">
                            <div className="hero-overlay" style={divStyle} />
                            <div className="inner width-2">
                                <div className="block _no-bg">
                                    <div className="box-Series">
                                        <div className="box-view row">
                                            <div className="box-image col-xl-8">
                                                <img  src={`https://image.tmdb.org/t/p/w500/${data.poster_path}` } alt="img_Series"/>
                                            </div>
                                            <div className="box-content col-xl-16">
                                                <h2 className="title-h2 box-title">{data.title} <span className="data-age">({releaseData.substring(0,4)})</span></h2>
                                                <p className="box-subtitle">{data.original_title} | {data.runtime} мин.</p>
                                                <div className="box-rating"><ReactStars count={10} value={data.vote_average}  edit={false} size={28} color2={'#ffd700'} /></div>
                                                <div className="box-item">
                                                    <p className="box-subtitle">Обзор</p>
                                                    <p className="box-desc">{data.overview}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="block p-0">
                        <div className="inner">
                            <div className="row columns-spaced">
                                <div className="col-xl-16">

                                </div>

                                <div className="col-xl-8">
                                    <div className="row flow-text">
                                        <div className="col s6 flow-text">
                                            <div className="card-panel">
                                                <p>Слоган: {data.tagline}</p>
                                                <p>Бюджет: {data.budget}$</p>
                                                <p>Дата релиза: {data.release_date }</p>
                                                <div>Компании производители: {data.production_companies.map((item,index) => ( <div key={index} className="chip"> {item.name} </div>))}</div>

                                                <p className="box-subtitle">Жанр:</p>
                                                <div className="box-genres">{data.genres.map((item,index) => ( <div key={index} className="item"> {item.name} </div>))}</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            )
        }  else return ( <Loader/> )

    }

}

Series.propTypes  = {
    Series: PropTypes.object,
    getSeries: PropTypes.func,
    id: PropTypes.string
};

export default Series;