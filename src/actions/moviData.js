import axios from 'axios';

const key = "api_key=51ef18ccb47b8598ee5b5a85d39c3242";
const keyV4 = "api_key=51ef18ccb47b8598ee5b5a85d39c3242";


const ActionCreator = {

    getListMovie: (page) => {
        return function (dispatch, getState) {
            dispatch({
                type: "REQUEST_LIST"
            });
            // axios.get(`https://api.themoviedb.org/3/discover/movie?${key}&page=${page}&language=ru`)
            axios.get(`https://api.themoviedb.org/3/discover/movie?${key}&language=ru&page=${page}`)
                .then(function (response) {
                    // console.log(response.data);
                    dispatch({
                        type: "RESPONSE_LIST",
                        data: response.data
                    });
                });
        };
    },

    getMovie: (id) => {
        return function (dispatch, getState) {
            dispatch({
                type: "REQUEST_MOVIE"
            });
            axios.get(`https://api.themoviedb.org/3/movie/${id}?${key}&&language=ru`)
                .then(function (response) {
                    // console.log(response.data);
                    dispatch({
                        type: "RESPONSE_MOVIE",
                        data: response.data
                    });
                });
        };
    },

    getListSeries: (page) => {
        return function (dispatch, getState) {
            dispatch({
                type: "REQUEST_LIST"
            });
            axios.get(`https://api.themoviedb.org/3/discover/tv?${key}&language=ru&page=${page}`)
                .then(function (response) {
                    // console.log(response.data);
                    dispatch({
                        type: "RESPONSE_LIST",
                        data: response.data
                    });
                });
        };
    },

    getSeries: (id) => {
        return function (dispatch, getState) {
            dispatch({
                type: "REQUEST_MOVIE"
            });
            axios.get(`https://api.themoviedb.org/3/tv/${id}?${key}&language=ru`)
                .then(function (response) {
                    // console.log(response.data);
                    dispatch({
                        type: "RESPONSE_MOVIE",
                        data: response.data
                    });
                });

        };
    }

};

export default ActionCreator;
