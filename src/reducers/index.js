import { combineReducers } from 'redux';


const dataListMovie = {
    loading: false,
    loaded: false,
    data: [],
    errors: [],
    page: 1,
    totalpages: 1

};

const dataMovie = {
    loading: false,
    loaded: false,
    data: [],
    errors: []

};

const dataListSeries = {
    loading: false,
    loaded: false,
    data: [],
    errors: [],
    page: 1,
    totalpages: 1

};

const dataSeries = {
    loading: false,
    loaded: false,
    data: [],
    errors: []

};

function ListMovie(state = dataListMovie, action){
    switch( action.type ){
        case "REQUEST_LIST":
            return {
                ...state,
                loading:true
    };

    case "RESPONSE_LIST":
        return {
            ...state,
            data: action.data.results,
            page: action.data.page,
            totalpages: action.data.total_pages,
            loading:false,
            loaded: true,
    };

    case "ERROR_LIST":
        return {
            ...state,
            errors:action.error
    };

    default:
        return state;
    }
}

function Movie(state = dataMovie, action){
    switch( action.type ){
        case "REQUEST_MOVIE":
            return {
                ...state,
                loading:true
        };

        case "RESPONSE_MOVIE":
            return {
                ...state,
                data: action.data,
                loading:false,
                loaded: true,
        };

        case "ERROR_MOVIE":
            return {
                ...state,
                errors:action.error
        };

        default:
            return state;
    }
}

function ListSeries(state = dataListSeries, action){
    switch( action.type ){
        case "REQUEST_LIST":
            return {
                ...state,
                loading:true
            };

        case "RESPONSE_LIST":
            return {
                ...state,
                data: action.data.results,
                page: action.data.page,
                totalpages: action.data.total_pages,
                loading:false,
                loaded: true,
            };

        case "ERROR_LIST":
            return {
                ...state,
                errors:action.error
            };

        default:
            return state;
    }
}

function Series(state = dataSeries, action){
    switch( action.type ){
        case "REQUEST_MOVIE":
            return {
                ...state,
                loading:true
            };

        case "RESPONSE_MOVIE":
            return {
                ...state,
                data: action.data,
                loading:false,
                loaded: true,
            };

        case "ERROR_MOVIE":
            return {
                ...state,
                errors:action.error
            };

        default:
            return state;
    }
}

const reducer = combineReducers({
    ListMovie,
    Movie,
    ListSeries,
    Series
});

export default reducer;
