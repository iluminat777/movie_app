import Series from '../components/Series';
import { connect } from 'react-redux'
import ActionCreator from '../actions/moviData';

const mapStateToProps = (state, ownProps) => {
    return {
        Series: state.Series
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getSeries: (id) => {
            dispatch( ActionCreator.getSeries(id) );
        }
    };
};

const MySeries = connect(
    mapStateToProps,
    mapDispatchToProps
)(Series);


export default MySeries;
