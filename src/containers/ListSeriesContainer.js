import ListSeries from '../components/ListSeries';
import { connect } from 'react-redux'
import ActionCreator from '../actions/moviData';

const mapStateToProps = (state, ownProps) => {
    return {
        ListSeries: state.ListSeries
    };
};

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getListSeries: (page) => {
            dispatch( ActionCreator.getListSeries(page) );
        }
    };
};

const MyListSeries = connect(
    mapStateToProps,
    mapDispatchToProps
)(ListSeries);


export default MyListSeries;
