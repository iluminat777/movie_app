'use strict';

// Case main-module init
// It is a global var
require('./gulp/case');

// Include Gulp & Tools We'll Use
const gulp              = require('gulp');
// const path              = require('path');
// const mergeStream       = require('merge-stream');
// const runSequence       = require('run-sequence');
// const browserSync       = require('browser-sync').create();
// const gulpif            = require('gulp-if');
// const debug             = require('gulp-debug');
// const chalk             = require('chalk');
// const notify            = require('gulp-notify');
// const plumber           = require('gulp-plumber');
// const rename            = require('gulp-rename');
// const rimraf            = require('rimraf');
// const watch             = require('gulp-watch');
// const globWatch         = require('glob-watcher');
// const gUtil             = require('gulp-util');
// const cached            = require('gulp-cached');
// const fs                = require('fs');
// const insert            = require('gulp-insert');
// const prettyTime        = require('pretty-hrtime');
// const glob              = require('glob');

// const sourcemaps        = require('gulp-sourcemaps');
// const gulpLoadPlugins   = require('gulp-load-plugins');

// CSS
// const autoprefixer      = require('gulp-autoprefixer');
// const csscomb           = require('gulp-csscomb');
// const cssnano           = require('gulp-cssnano');
// const gmmq               = require('gulp-merge-media-queries');

// Sass less styles postCSS
// const importFile        = require('gulp-file-include');
// const sass              = require('gulp-sass');
// const sassInheritance   = require('gulp-sass-inheritance');
// const sassGraph         = require('sass-graph');
// const less              = require('gulp-less');
// const stylus            = require('gulp-stylus');
// const postcss           = require('gulp-postcss');

// HTML
// const htmlv             = require('gulp-html-validator');
// const template          = require('gulp-template');
// const useref            = require('gulp-useref');
// const htmlmin           = require('gulp-htmlmin');
// const img64Html         = require('gulp-img64-html');
// const entities          = require("entities");

// Jade Pug slim twig
// const jade              = require('gulp-jade');
// const pug                  = require('gulp-pug');
// const haml              = require('gulp-haml');
// const twig              = require('gulp-twig');
// const slim              = require('gulp-slim');

// JS
//Validation
// const eslint            = require('gulp-eslint');

// Image
// const imagemin          = require('gulp-imagemin');
// const svgmin            = require('gulp-svgmin');
// const base64            = require('gulp-base64');
// const size              = require('gulp-size');

// Sprite
// const svg2png           = require('gulp-svg2png');
// const svgSymbols        = require('gulp-svg-symbols');
// const svgSprite         = require('gulp-svg-sprite');
// const pngSprite         = require('gulp.spritesmith');

// Optimization
// const combiner          = require('stream-combiner2').obj;
// const cache             = require('gulp-cache');
// const remember          = require('gulp-remember');
// const cached            = require('gulp-cached');
// const filter            = require('gulp-filter');
// const newer             = require('gulp-newer');
// minify               = require('gulp-minify');
// const newer_sass        = require("gulp-newer-sass.js");
// const requireDir        = require('require-dir');


// Dev task.
gulp.task('style', gulp.series('style:dev', 'watch:style', 'browsersync'));

// Register links to main tasks without namespace
// Build-dev task. Build dev-version (without watchers)
gulp.task('build-dev', gulp.series('style:dev', 'watch:style'));

// Dev task. Build dev-version with watchers and livereload
// gulp.task('dev', () => gulp.start('main:dev'));
//
// // Build task. Build release version
// gulp.task('build', () => gulp.start('main:build'));
//
// // Init task. Just start init task
// gulp.task('init', () => gulp.start('service:init'));
//
// // Re-init task. Just start re-init task
// gulp.task('re-init', () => gulp.start('service:re-init'));
//
// // Update-deps task. Just start update-deps task
// gulp.task('update-deps', () => gulp.start('service:update-deps'));
//
// // Default task. Just start build task
// gulp.task('default', () => gulp.start('build'));

/*
 * Define default task that can be called by just running `gulp` from cli
 */
gulp.task('default', gulp.series('style:dev'));